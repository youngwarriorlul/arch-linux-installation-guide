START

Welcome to this tutorial on how to install arch linux.

SSH


when in arch flash drive
* ip addr

under enp0s3, Inet, that’s the ip, remember it (something like 192.168.0.12?)

setup password now for ssh to work
* passwd
start ssh service
* systemctl start sshd

connect to that ip (home)

termux
on a phone (termux)
* pkg install openssh
* ssh root@192.68.0.12 (example)

* timedatectl set-ntp true
* lsblk

 (partition table)
see what disk(s) to format, sda will be an example name in this guide (sda = disk name)

* cfdisk /dev/sda

most likely the bios is in UEFI mode so go for the gpt option example layout



                [        New        ]        550M                [        Type        ]        EFI Systeem
                [        New        ]        35G                 [        Type        ]        Linux filesystem
                [        New        ]        (the rest)          [        Type        ]        Linux filesystem
                [        Write      ]        yes                 [        Quit        ]
                


FORMAT

fat for general os compatibility on a boot drive, ext4 is a safe bet, btrfs is ok too
* mkfs.fat        -F32 /dev/sda1
* mkfs.ext4 /dev/sda2
* mkfs.ext4 /dev/sda3


MOUNT TIME

* mount /dev/sda2 /mnt
* mkdir -p /mnt/boot/efi
* mount /dev/sda1 /mnt/boot/efi
* mkdir /mnt/home
* mount /dev/sda3 /mnt/home
* lsblk


INSTALL THE SYSTEM

* pacstrap /mnt base base-devel linux linux-firmware vim nano


STAB

* genfstab -U /mnt >> /mnt/etc/fstab


ARCH-CHROOT INTO IT

* arch-chroot /mnt /bin/bash

* pacman -S networkmanager grub efibootmgr os-prober
* systemctl enable NetworkManager
* grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi --no-nvram --removable
* grub-mkconfig -o /boot/grub/grub.cfg


SET PASSWORD

* passwd


SET LOCALE

english_us for example (I’m just used to that keyboard layout)
* vim /etc/locale.gen

uncouncomment en_US,

* locale-gen
* echo LANG=en_US.UTF-8 > /etc/locale.conf


SET HOSTNAME

Enter a name of the computer
* echo insert_name > /etc/hostname


CHECK THE INSTALL

* mkinitcpio -P


ADD USER

* useradd -mg wheel,storage,disk  username
* passwd username


add the user to sudoers

* visudo
* /wheel

change
#%wheel
to
 %wheel


DESKTOP

* pacman -S mesa xorg lightdm lightdm-gtk-greeter xfce4 xfce4-goodies firefox terminator i3 polybar dunst rofi picom 

                (replace mesa with nvidia nvidia-utils if nvidia card)
                press enter to go with the defaults
* systemctl enable lightdm
after everything is done
* exit
* umount -R /mnt
* reboot


MODERN VULKAN FOR DXVK

enable multilib (go in terminal or follow the path in a file manager)
* vim /etc/pacman.conf
* [multilib]
* Include = /etc/pacman.d/mirrorlist
		 uncomment those two


AMD

* sudo pacman -S lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader



AUR

* git clone https://aur.archlinux.org/paru.git
* cd paru
* makepkg -si


END

* exit
* umount -R /mnt
* reboot
